# TODO List

Hardware:
  [ ] Create hand-solder footprint for U2
  [ ] Improve silk-screen to document debug header.
  [ ] Improve silk-screen to document expansion pins.
  [ ] Improve silk-screen to document CAN H, CAN L and GND.
  [ ] Improve silk-screen to document termination jumper.
  [ ] Combine decoupling capacitors which are close together?

